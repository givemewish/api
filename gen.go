//go:generate protoc --proto_path=lk/lkservice  --proto_path=thirdparty --go_out=plugins=grpc:lk/lkservice --grpc-gateway_out=logtostderr=true:lk/lkservice --swagger_out=logtostderr=true:lk/lkservice lkservice.proto
//go:generate protoc --proto_path=lk/lkservice -I=thirdparty --go_out=plugins=grpc:lk/lkservice --grpc-gateway_out=logtostderr=true:lk/lkservice --swagger_out=logtostderr=true:lk/lkservice lkservice.proto

package api
