# API

## MVP

### Желания на главной странице (GET /main/wishes)
``` json
req:
{
  "limit": 15,
  "offset": 0,
  "order_by": "string"// asc | desc
}

resp:
{
  "wishes": [
    {
      "wish_id": "string",
      "body": {
        "wish_id": "string",
        "title": "string",
        "description": "string",
        "price": 0,
        "tags": [
          "string"
        ],
        "link": "string",
        "photo_id": "string", // url uuid
        "bookings": [
          {
            "user_id": "string",
            "photo_id": "string"
          }
        ]
      }
    }
  ]
  "total": 12313
}
```

### Поиск желаний (GET /wishes/search)
``` json
req:
{
  "search": "string",
  "limit": 10,
  "offset": 0
}

resp:
{
  "wishes": [
    {
      "wish_id": "string",
      "body": {
        "wish_id": "string",
        "title": "string",
        "description": "string",
        "price": 0,
        "tags": [
          "string"
        ],
        "link": "string",
        "photo_id": "string",
        "bookings": [
          {
            "user_id": "string",
            "photo_id": "string"
          }
        ]
      }
    }
  ],
  "total": 0
}
```

### Список моих друзей (GET /users/i/friends/my)
``` json
req:
{
  "limit": 20,
  "offset": 0
}

resp:
{
  "users": [
    {
      "user_id": "string",
      "name": "string",
      "birthday": "2023-11-15T15:38:59.716Z",
      "days_to_birthday": 2,
      "photo_id": "string",
      "count_wishes": 0
    }
  ],
  "total": 54325
}
```

### Профиль пользователя по ID (/users/{user_id}/info)
``` json
req:
{
  "user_id": "string"
}

resp:
{
  "user_id": "string",
  "name": "string",
  "login": "string",
  "birthday": "2023-11-15T15:48:43.913Z",
  "email": "string",
  "photo_id": "string",
  "accepted": "NOT_SUBSCRIBE"//NOT_SUBSCRIBE | SUBSCRIBED | FRIEND
}
```

### Желания пользователя по ID пользователя (/users/{user_id}/wishes)
``` json
req:
{
  "user_id": "string",
  "limit": 15,
  "offset": 0,
  "order_by": "string"// asc | desc
}

resp:
{
  "wishes": [
    {
      "wish_id": "string",
      "body": {
        "wish_id": "string",
        "title": "string",
        "description": "string",
        "price": 0,
        "tags": [
          "string"
        ],
        "link": "string",
        "photo_id": "string", // url
        "bookings": [
          {
            "user_id": "string",
            "photo_id": "string"
          }
        ]
      }
    }
  ]
  "total": 12313
}
```

### Добавление желания (POST /wishes/add)
``` json
req:
{
  "title": "string",
  "description": "string",
  "price": 0,
  "tags": [
    "string"
  ],
  "link": "string",
  "photo_id": "string"
}

resp:
{
  "wish_id": "string"
}
```

### Мои желания (GET /users/i/wishes/my)
``` json
req:
{
  "limit": 15,
  "offset": 0,
  "order_by": "string"// asc | desc
}

resp:
{
  "wishes": [
    {
      "wish_id": "string",
      "body": {
        "wish_id": "string",
        "title": "string",
        "description": "string",
        "price": 0,
        "tags": [
          "string"
        ],
        "link": "string",
        "photo_id": "string", // url uuid
        "bookings": [
          {
            "user_id": "string",
            "photo_id": "string"
          }
        ]
      }
    }
  ]
  "total": 12313
}
```

### Отправить фотографию на сервер (POST /tools/upload)
``` json
req:
{
  "photo_base64": "string"
}

resp:
{
  "photo_id": "string"
}
```

### Скачать фотографию с сервера (POST /tools/download)
``` json
req:
{
  "photo_id": "string",
  "size": "string"
}

resp:
{
  "photo_base64": "string"
}
```
